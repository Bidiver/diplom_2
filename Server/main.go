package main

import (
	"Zapis_na_priyom/Config"
	"Zapis_na_priyom/Models"
	"Zapis_na_priyom/Routes"
	"log"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

func main() {
	var err error

	conn := "host=localhost user=postgres password=postgres dbname=Zapis_na_priyom sslmode=disable"
	Config.DB, err = gorm.Open(postgres.Open(conn), &gorm.Config{})
	if err != nil {
		log.Fatal(err)
	}

	Config.DB.AutoMigrate(
		&Models.Zapis_na_priyom{})

	Models.DataFilling()

	r := gin.Default()

	r.Static("/public", "../Client/public")
	r.LoadHTMLFiles("../Client/public/main.html")

	r.GET("/", Routes.Root)

	r.GET("/zapis", Routes.GetZapis_na_priyom)

	r.PUT("/zapis", Routes.CreateZapis_na_priyom)

	r.Run(":7000")
}
