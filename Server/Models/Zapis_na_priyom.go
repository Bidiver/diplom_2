package Models

import (
	"time"
)

type Zapis_na_priyom struct {
	ID     int       `json:"id" gorm:"autoIncrement"`
	DVZ    time.Time `json:"dvz"`
	DIVZNP time.Time `json:"divznp"`
	FIO    string    `json:"fio"`
	Tel    string    `json:"tel"`
	NapOb  string    `json:"napob"`
	Ptim   string    `json:"ptim"`
}
