package Models

import (
	"Zapis_na_priyom/Config"
	"math/rand"
	"time"
)

func randate() time.Time {
	min := time.Date(2022, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	max := time.Date(2023, 1, 0, 0, 0, 0, 0, time.UTC).Unix()
	delta := max - min

	sec := rand.Int63n(delta) + min
	return time.Unix(sec, 0)
}

func DataFilling() {
	Config.DB.Create(&Zapis_na_priyom{ID: 1, DVZ: randate(), DIVZNP: randate(), FIO: "Юрий Савцов Олегович", Tel: "+799123213", NapOb: "Социальная помощь 5 кабинет", Ptim: "Нет примечаний"})
}
