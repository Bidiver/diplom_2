package Routes

import (
	"Zapis_na_priyom/Config"
	"Zapis_na_priyom/Models"
	"log"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
)

type Zapis_na_priyomDTO struct {
	ID     int    `json:"id"`
	DVZ    string `json:"dvz"`
	DIVZNP string `json:"divznp"`
	FIO    string `json:"fio"`
	Tel    string `json:"tel"`
	NapOb  string `json:"napob"`
	Ptim   string `json:"ptim"`
}

func GetZapis_na_priyom(context *gin.Context) {
	var zapis []Models.Zapis_na_priyom
	Config.DB.Find(&zapis)

	context.JSON(http.StatusOK, zapis)
}

func CreateZapis_na_priyom(c *gin.Context) {
	var err error
	var input Zapis_na_priyomDTO
	if err := c.ShouldBindJSON(&input); err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}
	log.Println(c.ShouldBindJSON(&input))

	var dvz time.Time
	if dvz, err = time.Parse("2006-01-02", input.DVZ); err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}

	var divznp time.Time
	if divznp, err = time.Parse("2006-01-02 15:04", input.DIVZNP); err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, err.Error())
		return
	}

	zapis := Models.Zapis_na_priyom{
		DVZ:    dvz,
		DIVZNP: divznp,
		FIO:    input.FIO,
		Tel:    input.Tel,
		NapOb:  input.NapOb,
		Ptim:   input.Ptim}
	if err = Config.DB.Create(&zapis).Error; err != nil {
		log.Println(err)
		c.String(http.StatusInternalServerError, err.Error())
	} else {
		c.JSON(http.StatusOK, gin.H{"id": zapis.ID})
	}
}
