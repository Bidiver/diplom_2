import * as REST from "./REST"
import {IZapis_na_priyom} from "./Models/interfaces";
import { Modal } from "bootstrap";

let zapis_na_priyom: IZapis_na_priyom[] = [];

const tabZapis_button_new = document.getElementById("tabZapis_button_new");

const modalZapis_input_DVZ = document.getElementById("modalZapis_input_DVZ") as HTMLInputElement;
const modalZapis_input_DIVZNP = document.getElementById("modalZapis_input_DIVZNP") as HTMLInputElement;
const modalZapis_input_Name = document.getElementById("modalZapis_input_Name") as HTMLInputElement;
const modalZapis_input_Tel = document.getElementById("modalZapis_input_Tel") as HTMLInputElement;
const modalZapis_input_NapOb = document.getElementById("modalZapis_input_NapOb") as HTMLInputElement;
const modalZapis_input_Ptim = document.getElementById("modalZapis_input_Ptim") as HTMLInputElement;
const modalZapis_button_save = document.getElementById("modalZapis_button_save");

const modalZapis = new Modal(document.getElementById("modalZapis"));

tabZapis_button_new.addEventListener("click", () => {
    modalZapis_input_DVZ.value = "";
    modalZapis_input_DIVZNP.value = "";
    modalZapis_input_Name.value = "";
    modalZapis_input_Tel.value = "";
    modalZapis_input_NapOb.value = "";
    modalZapis_input_Ptim.value = "";
    showModalZapis();
});

function showModalZapis(){
    let Zap: IZapis_na_priyom = null;

    modalZapis.show();
}

modalZapis_button_save.addEventListener("click", () => {
    let zapis: IZapis_na_priyom = {
       id: null,
       dvz: modalZapis_input_DVZ.value,
       divznp: modalZapis_input_DIVZNP.value,
       fio: modalZapis_input_Name.value,
       tel: modalZapis_input_Tel.value,
       napob: modalZapis_input_NapOb.value,
       ptim: modalZapis_input_Ptim.value
    }
    REST.PUT("/zapis", zapis).then((data: any) => {
        console.log("Заявка создана ID = ", data.id);
    })
    modalZapis.hide();
});