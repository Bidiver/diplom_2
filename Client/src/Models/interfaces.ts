export interface IZapis_na_priyom {
    id:            number
    dvz:       string
    divznp:    string
    fio:      string
    tel:     string
    napob:    string
    ptim: string
}